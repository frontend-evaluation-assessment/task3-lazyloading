import './App.css';
import React,{useState,useEffect} from 'react';
import { Heading } from './components/heading';
import { ImageList } from './components/imageList';
import { Loader } from './components/Loader';

import axios from 'axios'
import styled from 'styled-components';
import {createGlobalStyle} from 'styled-components'
import InfiniteScroll from 'react-infinite-scroll-component';
//style
const GlobalStyle = createGlobalStyle`
*{
  margin:0;
  padding:0;
  box-sizing:border-box;
}
body{
  font-family:san-serif;
}`;

const WrapperImage = styled.div`
 max-width:80%;
 height:auto;
 margin:10px auto;
 display:grid;
 grid-gap:10px;
 grid-template-columns:repeat(auto-fit, minmax(250px,1fr));
 grid-auto-rows:300px;
`;

function App() {
  const [images,setImages]=useState([]);

  useEffect(()=>{
    fetchImages();
  }, []);

  const fetchImages =() =>{
    const apiPage = "https://api.unsplash.com"
    axios.get(`${apiPage}/photos/random?client_id=7wmegj4rm1Shb5YpAoWuef23Xk9Jgq6qX41n6JDoZ5s&count=8`)
    .then(res => setImages([...images, ...res.data]))
  }
  return (
    <div className="App">
      <InfiniteScroll
     dataLength={images.length}
     next={fetchImages}
     hasMore={true}
     loader={<Loader/>}
     >
     <Heading/>
     <GlobalStyle/>
     <WrapperImage>
     {images.map(image => (
      <ImageList url={image.urls.thumb} myKey={image.id}/>
     ))}
     </WrapperImage>
     </InfiniteScroll>
    </div>
  );
}

export default App;
