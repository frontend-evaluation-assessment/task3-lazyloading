import React from 'react'
import styled from 'styled-components'

const Header = styled.header`
width:100%;
text-align:center;
margin-top:30px;
font-family:sans-serif;
`;

const Title = styled.h1`
font-size:3em;
font-weight:900;
margin-bottom:10px;
display: inline-block;
background:linear-gradient(90deg,#2ca2b4,#5598de 24%,#7f87ff 45%,#f65aad 76%,#ec3d43);
background-clip: text;
-webkit-background-clip: text;
-webkit-text-fill-color: transparent;
`;

export const Heading = () => {
  return (
    <Header>
        <Title>LAZY LOADING</Title>
        <p>To Avoid Pagination</p>
        <p><strong>Unsplash api</strong> is used in this to perform the lazy load. 
        </p>
        <h2>scroll to the end to see the lazy load</h2>
    </Header>
  )
}
